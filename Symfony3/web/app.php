<?php

use Symfony\Component\HttpFoundation\Request;

/**
 * @var Composer\Autoload\ClassLoader
 */
$loader = require __DIR__.'/../app/autoload.php';
include_once __DIR__.'/../var/bootstrap.php.cache';

// Enable APC for autoloading to improve performance.
// You should change the ApcClassLoader first argument to a unique prefix
// in order to prevent cache key conflicts with other applications
// also using APC.
/*
$apcLoader = new Symfony\Component\ClassLoader\ApcClassLoader(sha1(__FILE__), $loader);
$loader->unregister();
$apcLoader->register(true);
*/

/* Note if someone has an 404 error on the front page (Symfony is an awesome framework):

    http://stackoverflow.com/questions/16243746/starting-with-symfony-2-app-dev-php-works-app-php-404s


    I also had the same problem. I share how I solved it (Strange but it works).
    In file web/app.php change:

    $kernel = new AppKernel('prod', false);

    by

    $kernel = new AppKernel('prod', true); // Enable debug mode

    Then load the page in the browser (in app environment) and change the file again disabling debug:

    $kernel = new AppKernel('prod', false);

    Try loading the page in the browser again. It should work properly.
*/

$kernel = new AppKernel('prod', false);
$kernel->loadClassCache();
//$kernel = new AppCache($kernel);

// When using the HttpCache, you need to call the method in your front controller instead of relying on the configuration parameter
//Request::enableHttpMethodParameterOverride();
$request = Request::createFromGlobals();
$response = $kernel->handle($request);
$response->send();
$kernel->terminate($request, $response);
