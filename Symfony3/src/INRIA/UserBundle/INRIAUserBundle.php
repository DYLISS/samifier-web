<?php
// src/INRIA/UserBundle/INRIAUserBundle.php

namespace INRIA\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class INRIAUserBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
