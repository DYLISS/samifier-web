<?php
// src/INRIA/DylissmBundle/Controller/ExperimentController.php
namespace INRIA\DylissBundle\Controller;

use INRIA\DylissBundle\Entity\Experiment;
use INRIA\DylissBundle\Form\ExperimentType;
use INRIA\DylissBundle\Form\EditExperimentType;
//use INRIA\UserBundle\Entity\User;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
// Secure methods with annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class ExperimentController extends Controller
{

    // Secured experiment id
    private $exp_id;

    public function __construct()
    {
        // You CAN'T fix the repository here
        // Because constructor doesn't know anything about doctrine service here
        // (Not already injected) => Sorry you have to do ugly code => trololol !
        // PS : you can register a class as service... => byebye DRY philosophy !
    }

    protected function getSecuredExperimentId($id)
    {
        // Retrieve ids of experiments associated to the current user
        // Verify if the given id is in the list
        // If not, return the greatest id
        // If list is empty, return null

        $ids = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('INRIADylissBundle:Experiment')
            ->getAllUserIdExperiments($this->getUser());

        if (in_array($id, $ids)) // ok
            return $id;
        else { // abnormal, id not authorized
            // Print error
            $this->addFlash('danger', 'You don\'t have the right to delete this experiment.');
            return null;
        }
    }

    protected function getRepository()
    {
        return $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('INRIADylissBundle:Experiment');
    }

    protected function getAllUserExperiments()
    {
        // Return all experiments for the given user entity
        return $this->getRepository()
            ->getAllUserExperiments($this->getUser());
    }

    protected function getSrcFilesWithExperiments()
    {
        // Return data about the srcFile for each experiment
        return $this->getRepository()
            ->getSrcFilesWithExperiments($this->getUser());
    }

    protected function getMaximumAuthorizedExperiments()
    {
        // Return the number of experiments that a user can create.

        return $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('INRIADylissBundle:AdminConfig')
            ->getCurrentConfig()
            ->getNumberExperiments();
    }

    /**
    * @Security("has_role('ROLE_USER')")
    */
    public function viewAction()
    {
        // List passed experiments with data about them (files, checksums, dates, destinations)
        $experiments = $this->getSrcFilesWithExperiments();
        //var_dump($experiments);
        return $this->render('INRIADylissBundle:Experiment:index.html.twig',
                             array('experiments' => $experiments));
    }

    /**
    * @Security("has_role('ROLE_USER')")
    */
    public function addAction(Request $request)
    {

        // Verify if the user can create a new experiment
        // Admin user has no limitations !
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')
            AND (count($this->getAllUserExperiments()) >= $this->getMaximumAuthorizedExperiments())) {

            // Print error
            $this->addFlash('danger',
                            'You have exceeded the maximum of allowed experiments. '
                            . 'Please delete at least one experiment.');

            // Redirect to the list of experiments
            return $this->redirect($this->generateUrl('inria_dyliss_view_exp'));
        }


        // Create Experiment object
        $experiment = new Experiment();

        // Attach it to the current user
        $experiment->setUser($this->getUser());

        // Create Form
        $form = $this->createForm(ExperimentType::class, $experiment);

        // We do the link between Request <-> Form (Hydratation phase)
        // Now, $experiment contains values taken from user inputs.
        // Now if the form is valid, we can persist entities
        if ($form->handleRequest($request)->isValid()) {

            // We persist entities in database
            try {
                $em = $this->getDoctrine()->getManager();
                $em->persist($experiment);
                $em->flush();

                // Print success
                $this->addFlash('success', 'The experiment was successfully created.');

                // Redirect to the visualisation of the results for the new experiment
                return $this->redirect($this->generateUrl('inria_dyliss_view_result',
                                                        array('exp_id' => $experiment->getId())));

            } catch (\Exception $e) {

                // Print error
                $this->addFlash('danger',
                                'The creation of the experiment was refused. ' . $e->getMessage());

                // Redirect to the visualisation of the results for the new experiment
                return $this->redirect($this->generateUrl('inria_dyliss_view_exp'));
            }
        }

        // Here the form is not valid because :
        // GET query  => the user is incoming to this page
        // POST query => the form is invalid => display new form
        return $this->render('INRIADylissBundle:Experiment:add.html.twig',
                             array('form' => $form->createView()));
    }

    /**
    * @Security("has_role('ROLE_USER')")
    */
    public function modifyAction(Request $request, $exp_id)
    {

        // Detection of the greatest id allowed for this user
        // Verification of the given id
        //var_dump($exp_id);
        // Secure the given exp_id
        $this->exp_id = $this->getSecuredExperimentId($exp_id);
        //var_dump($exp_id);

        if ($exp_id !== $this->exp_id) {
            // Print information
            $this->addFlash('danger', 'You have selected an erroneous experiment.');
            return $this->redirect($this->generateUrl('inria_dyliss_view_exp'));
        }

        // Get Experiment object
        $experiment =  $this->getRepository()
                            ->find(intval($this->exp_id));

        // Get previous getDstFile
        $em = $this->getDoctrine()->getManager();
        $dstFile = $experiment->getDstFile();

        if ($dstFile !== null) {

             $em->remove($dstFile);
             // No flush here !
             // We don't want to remove this object if the forme is not valid !

             // The "restoration" of the destination file is made
             // if there is NO flush until the render of the page.

             // The Experiment still contains the destination file if
             // we don't fix it explicitly or if we don't flush.
             // Set dest file to null
             $experiment->setDstFile();
        }

        // Create Form
        $form = $this->createForm(EditExperimentType::class, $experiment);

        // We do the link between Request <-> Form (Hydratation phase)
        // Now, $experiment contains values taken from user inputs.
        // Now if the form is valid, we can persist entities
        if ($form->handleRequest($request)->isValid()) {

            // We persist entities in database
            try {

                // This flush suppress the previous dstFile
                // marked for removing and persists the new dstFile.
                // (it is an update of the current experiment)
                $em->flush();

                // Print success
                $this->addFlash('success', 'The experiment was successfully updated.');

                // Redirect to the visualisation of the results for the new experiment
                return $this->redirect($this->generateUrl('inria_dyliss_view_result',
                                                        array('exp_id' => $experiment->getId())));

            } catch (\Exception $e) {
                // PS: entity manager is closed here and all changes are rolled back.

                // Print error
                $this->addFlash('danger',
                                'The modification of the experiment was refused. ' . $e->getMessage());

                // Redirect to the list of experiments
                return $this->redirect($this->generateUrl('inria_dyliss_view_exp'));
            }
        }

        // Restoration of the destination file
        // Nothing to do => We must to not do flush()


        // Here the form is not valid because :
        // GET query  => the user is incoming to this page
        // POST query => the form is invalid => display new form
        return $this->render('INRIADylissBundle:Experiment:modify.html.twig',
                             array('form' => $form->createView(),
                                   'exp_id' => $this->exp_id));

    }

    /**
    * @Security("has_role('ROLE_USER')")
    */
    public function deleteAction($exp_id)
    {
        // Detection of the greatest id allowed for this user
        // Verification of the given id
        //var_dump($exp_id);
        // Secure the given exp_id
        $this->exp_id = $this->getSecuredExperimentId($exp_id);
        //var_dump($exp_id);

        if ($exp_id !== $this->exp_id) {
            // Print information
            $this->addFlash('danger', 'You have selected an erroneous experiment.');

        } else {
            // We get the entity in database
            $em = $this->getDoctrine()->getManager();

            // We erase this entity
            $em->remove($em->find('INRIADylissBundle:Experiment', $exp_id));
            $em->flush();
        }

        // Redirect to the list of experiments
        return $this->redirect($this->generateUrl('inria_dyliss_view_exp'));
    }

    /**
    * @Security("has_role('ROLE_USER')")
    */
    public function validateAction($exp_id)
    {
        // Detection of the greatest id allowed for this user
        // Verification of the given id
        $this->exp_id = $this->getSecuredExperimentId($exp_id);

        if ($exp_id !== $this->exp_id) {
            // Print information
            $this->addFlash('danger', 'You have selected an erroneous experiment.');

        } else {
            // Contact the API and retrieve data from it
            $data = $this->get('inria_dyliss.api_matching_kernel')
                     ->validate($this->exp_id);

            // Print information
            $this->addFlash('success', 'Experiment was successfully validated.');
        }

        // Redirect to the list of experiments
        return $this->redirect($this->generateUrl('inria_dyliss_view_exp'));
    }
}


