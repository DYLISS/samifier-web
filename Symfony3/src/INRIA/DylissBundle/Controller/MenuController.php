<?php
// src/INRIA/DylissmBundle/Controller/MenuController.php
namespace INRIA\DylissBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
// Secure methods with annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class MenuController extends Controller
{

    // Secured experiment id
    private $exp_id;

    // Used to secure tabs and display the view in viewAction()
    // Same in ResultController
    private $authorized_tabs = array('matched' => '#matched_content',
                                     'proposal' => '#proposal_content',
                                     'tobematched' => '#tobematched_content');


    private function getSecuredExperimentId($id)
    {
        // Retrieve ids of experiments associated to the current user
        // Verify if the given id is in the list
        // If not, return the greatest id
        // If list is empty, return null

        $ids = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('INRIADylissBundle:Experiment')
            ->getAllUserIdExperiments($this->getUser());

        if (empty($ids)) { // empty
            // Print information
            $this->addFlash('info', 'There is no current experiment.');
            return null;

        } elseif (in_array($id, $ids)) { // ok
            return $id;

        } elseif (empty($id) or ($id == '')) { // null or empty
            return $ids[0];

        } else { // abnormal, id not authorized
            // Print error
            $this->addFlash('danger', 'You don\'t have the right to view these results.');
            return $ids[0];
        }
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__ . '/../../../../web/' . $this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        $user = $this->container->get('security.token_storage')->getToken()->getUser();

        return 'uploads/' . $user->getUsernameCanonical();
    }

    private function openLogFile()
    {
        // Open the logfile of the experiment
        // This is used to fill the textarea ine the view of the experiment.

        $user = $this->get('security.token_storage')
                     ->getToken()
                     ->getUser();

        $log_file = $this->getUploadRootDir() . '/' . $this->exp_id . '/log.txt';

        try {
            $log_file_content = file_get_contents($log_file, FILE_USE_INCLUDE_PATH);
            return ($log_file_content === FALSE) ? 'No current logfile.' : $log_file_content;
        } catch (\Exception $e) {
            return 'No current logfile.';
        }
    }

    /**
    * @Security("has_role('ROLE_USER')")
    */
    public function menuAction($exp_id)
    {
        // Display the Menu with statistics & controls
        // Take the experiment id & logs attached to it

        // Secure the given exp_id
        $this->exp_id = $this->getSecuredExperimentId($exp_id);

        return $this->render('INRIADylissBundle:Result:menu.html.twig',
                             array('exp_id' => $this->exp_id,
                                   'log' => $this->openLogFile()
                             ));
    }

    /**
    * @Security("has_role('ROLE_USER')")
    */
    public function nameMatchingAction($exp_id, $method, $biological_object_type)
    {
        // This function executes name matching methods
        // The given method could be "exact" or "inferred".
        // The first gives exact matching whereas the second gives similarity scores.

        // Secure the given exp_id
        $this->exp_id = $this->getSecuredExperimentId($exp_id);

        // If the given id is different from the verified id,
        // we don't take the risk to modify an erroneous association.
        // => instant return

        if ($exp_id != $this->exp_id) {
            // Print information
            $this->addFlash('danger', 'You have selected an erroneous experiment.');
            // Redirect to results view
            return $this->redirectToRoute('inria_dyliss_view_result',
                                      array('exp_id' => $this->exp_id,
                                      ), 301);
        }

        // TODO: recup la bdd interrogée
        // Contact the API and retrieve data from it
        $data = $this->get('inria_dyliss.api_matching_kernel')
                     ->nameMatching($this->exp_id,
                                    $method,
                                    $biological_object_type);

        if (isset($data['error'])) {
            // Print information
            $this->addFlash('danger', $data['error']);
        }

        // Redirect to results view
        return $this->redirectToRoute('inria_dyliss_view_result',
                                      array('exp_id' => $this->exp_id,
                                      ), 301);
    }

    /**
    * @Security("has_role('ROLE_USER')")
    */
    public function getInValidationAction($exp_id)
    {
        // This function executes name matching methods
        // The given method could be "exact" or "inferred".
        // The first gives exact matching whereas the second gives similarity scores.

        // Secure the given exp_id
        $this->exp_id = $this->getSecuredExperimentId($exp_id);

        // If the given id is different from the verified id,
        // we don't take the risk to modify an erroneous association.
        // => instant return

        if ($exp_id != $this->exp_id) {
            // Print information
            $this->addFlash('danger', 'You have selected an erroneous experiment.');
            // Redirect to results view
            return $this->redirectToRoute('inria_dyliss_view_result',
                                          array('exp_id' => $this->exp_id,
                                          ), 301);
        }

        // TODO: recup la bdd interrogée
        // Contact the API and retrieve data from it
        $data = $this->get('inria_dyliss.api_matching_kernel')
                     ->getInValidation($this->exp_id);

        if (isset($data['error'])) {
            // Print information
            $this->addFlash('danger', $data['error']);
        }

        // Redirect to results view
        return $this->redirectToRoute('inria_dyliss_view_result',
                                      array('exp_id' => $this->exp_id,
                                      ), 301);
    }

    /**
    * @Security("has_role('ROLE_USER')")
    */
    public function exactIdMatchingAction($exp_id)
    {
        // This function executes name matching methods
        // The given method could be "exact" or "inferred".
        // The first gives exact matching whereas the second gives similarity scores.

        // Secure the given exp_id
        $this->exp_id = $this->getSecuredExperimentId($exp_id);

        // If the given id is different from the verified id,
        // we don't take the risk to modify an erroneous association.
        // => instant return

        if ($exp_id != $this->exp_id) {
            // Print information
            $this->addFlash('danger', 'You have selected an erroneous experiment.');
            // Redirect to results view
            return $this->redirectToRoute('inria_dyliss_view_result',
                                          array('exp_id' => $this->exp_id,
                                          ), 301);
        }

        // TODO: recup la bdd interrogée
        // Contact the API and retrieve data from it
        $data = $this->get('inria_dyliss.api_matching_kernel')
                     ->exactIdMatching($this->exp_id);

        if (isset($data['error'])) {
            // Print information
            $this->addFlash('danger', $data['error']);
        }

        // Redirect to results view
        return $this->redirectToRoute('inria_dyliss_view_result',
                                      array('exp_id' => $this->exp_id,
                                      ), 301);
    }

    /**
    * @Security("has_role('ROLE_USER')")
    */
    public function samirMatchingAction($exp_id)
    {
        // This function executes Samir matching method for reactions

        // Secure the given exp_id
        $this->exp_id = $this->getSecuredExperimentId($exp_id);

        // If the given id is different from the verified id,
        // we don't take the risk to modify an erroneous association.
        // => instant return

        if ($exp_id != $this->exp_id) {
            // Print information
            $this->addFlash('danger', 'You have selected an erroneous experiment.');
            // Redirect to results view
            return $this->redirectToRoute('inria_dyliss_view_result',
                                          array('exp_id' => $this->exp_id,
                                          ), 301);
        }

        // Contact the API and retrieve data from it
        $data = $this->get('inria_dyliss.api_matching_kernel')
                     ->samirMatching($this->exp_id);

        if (isset($data['error'])) {
            // Print information
            $this->addFlash('danger', $data['error']);
        }

        // Redirect to results view
        return $this->redirectToRoute('inria_dyliss_view_result',
                                      array('exp_id' => $this->exp_id,
                                      ), 301);
    }

    /**
    * @Security("has_role('ROLE_USER')")
    */
    public function getLogFileAction($exp_id)
    {
        // This function allows the user to download the logfile attached to
        // the given experiment.

        // Secure the given exp_id
        $this->exp_id = $this->getSecuredExperimentId($exp_id);

        // If the given id is different from the verified id,
        // we don't take the risk to modify an erroneous association.
        // => instant return

        if ($exp_id != $this->exp_id) {
            // Print information
            $this->addFlash('danger', 'You have selected an erroneous experiment.');
            // Redirect to results view
            return $this->redirectToRoute('inria_dyliss_view_result',
                                      array('exp_id' => $this->exp_id,
                                      ), 301);
        }

        // Try to load the file
        try {
            $log_file_content = file_get_contents(
                $this->getUploadRootDir() . '/' . $this->exp_id . '/log.txt',
                FILE_USE_INCLUDE_PATH);

            if ($log_file_content === FALSE)
                throw new \Exception('No current logfile.');

        } catch (\Exception $e) {
            $this->addFlash('danger', 'No current logfile.');

            // Redirect to results view
            return $this->redirectToRoute('inria_dyliss_view_result',
                                      array('exp_id' => $this->exp_id,
                                      ), 301);
        }


        // Modification of the content-type to force the download;
        // Without that, the browser will try to open it.
        $response = new Response();
        $response->setContent($log_file_content);
        $response->headers->set('Content-Type', 'application/force-download');
        $response->headers->set('Content-disposition', 'filename=log_' . $this->exp_id . '.txt');

        return $response;
    }

}