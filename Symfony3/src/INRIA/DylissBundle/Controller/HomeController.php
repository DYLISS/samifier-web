<?php

namespace INRIA\DylissBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class HomeController extends Controller
{
    public function indexAction()
    {
      return $this->render('INRIADylissBundle:Home:index.html.twig');
    }

    public function viewhelpAction()
    {
      return $this->render('INRIADylissBundle:Help:index.html.twig');
    }
}
