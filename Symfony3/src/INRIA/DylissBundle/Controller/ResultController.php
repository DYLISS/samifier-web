<?php
// src/INRIA/DylissmBundle/Controller/ResultController.php
namespace INRIA\DylissBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
// Secure methods with annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class ResultController extends Controller
{

    // Used to secure tabs and display the view in viewAction()
    // Same in MenuController
    private $authorized_tabs   = array('matched'     => '#matched_content',
                                       'proposal'    => '#proposal_content',
                                       'tobematched' => '#tobematched_content');

    // Used to contact the API securely in getDataTabAction()
    private $authorized_status = array('matched'     => 'already matched',
                                       'proposal'    => 'proposal',
                                       'tobematched' => 'to be matched');

    // Don't forget to modify routing.yml settings
    private $authorized_formats = array('html', 'json', 'csv', 'sbml');

    // Secured experiment id
    private $exp_id;


    private function modifyAssociation($url_api, $exp_id, $assoc_id)
    {
        // Secure the given exp_id
        $this->exp_id = $this->getSecuredExperimentId($exp_id);

        // If the given id is different from the verified id,
        // we don't take the risk to modify an erroneous association.
        // => instant return

        if ($exp_id != $this->exp_id) {
            // Print information
            $this->addFlash('danger', 'You have selected an erroneous experiment.');
            throw new NotFoundHttpException('Error : ' . $this->exp_id . ';' . $assoc_id);
        }

        // Contact the API and retrieve data from it
        $json = $this->get('inria_dyliss.api_matching_kernel')
                     ->modifyAssociation($this->exp_id,
                                         $url_api,
                                         $assoc_id);

        // Print information
        if ($json['error'] == 'success') {
            $this->addFlash('success', 'Association successfully modified.');
        } else {
            $this->addFlash('danger', 'An error occurs : ' . $json['error']);
        }

        // Return basic response
        return new Response('ok');
    }


    private function getSecuredExperimentId($id)
    {
        // Retrieve ids (in DESC order) of experiments associated to the current user
        // Verify if the given id is in the list
        // If not, return the greatest id
        // If list is empty, return null

        $ids = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('INRIADylissBundle:Experiment')
            ->getAllUserIdExperiments($this->getUser());

        if (empty($ids)) { // empty
            // Print information
            $this->addFlash('info', 'There is no current experiment.');

            // I want 1 identical item at a time please !
            $bag_content = $this->get('session')->getFlashBag()->peek('info');
            $this->get('session')->getFlashBag()
                                 ->set('info',
                                       array_unique($bag_content));
            return null;

        } elseif (in_array($id, $ids)) { // ok
            return $id;

        } elseif (empty($id) or ($id == '')) { // null or empty
            return $ids[0];

        } else { // abnormal, id not authorized
            // Print error
            $this->addFlash('danger', 'You don\'t have the right to view these results.');
            return $ids[0];
        }
    }


    private function dataToCsv($data) {

        // Basic formating in csv fileformat
        // Here we return only the ids of elements/associations
        // Headers: SOURCE DESTINATION FORMULA ECNUMBER
        $result = "SOURCE;DESTINATION;FORMULA;ECNUMBER\n";

        foreach($data as $association) {
            // Source id
            $result .= $association['src']['id'];
            // Destination id if set
            $result .= (isset($association['dst']['id'])) ? ';' . $association['dst']['id'] : ';';
            // Formula if set
            $result .= (isset($association['dst']['formula'])) ? ';' . $association['dst']['formula'] : ';';
            // EC Number if set
            $result .= (isset($association['dst']['ecnumber'])) ? ';' . $association['dst']['ecnumber'] : ';';
            $result .= "\n";
        }

        return $result;
    }


    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__ . '/../../../../web/' . $this->getUploadDir();
    }


    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        $user = $this->container->get('security.token_storage')->getToken()->getUser();

        return 'uploads/' . $user->getUsernameCanonical();
    }

/*
    public function __call($method, $args)
    {
        if(is_callable(array($this, $method))) {
            return call_user_func_array($this->$method, $args);
        }
        // else throw exception
    }

    private function my_sorter($a, $b) {
        $c = strcmp($a['src']['id'], $b['src']['id']);
        if($c != 0) {
            return $c;
        }

        $c = strcmp($a['dst']['id'], $b['dst']['id']);
        if($c != 0) {
            return $c;
        }

        return strcmp($a['sim_score'], $b['sim_score']);
    }
*/

    /**
    * @Security("has_role('ROLE_USER')")
    */
    public function viewAction($exp_id)
    {
        // Detection of the greatest exp_id allowed for this user
        // Verification of the given exp_id
        //var_dump($exp_id);
        $this->exp_id = $this->getSecuredExperimentId($exp_id);
        // var_dump($this->exp_id);

        return $this->render('INRIADylissBundle:Result:index.html.twig',
                             array('authorized_tabs' => $this->authorized_tabs,
                                   'exp_id' => $this->exp_id
                             ));
    }


    // AJAX
    // TODO: capturer les erreurs d'authentification et ne pas renvoyer la page de login là dedans...
    /**
    * @Security("has_role('ROLE_USER')")
    */
    public function getDataTabAction($exp_id, $tab, $format)
    {
        // Display data for each tab of experiment


        // Name of given tab is not authorized !
        if (!array_key_exists($tab, $this->authorized_tabs) OR !in_array($format, $this->authorized_formats))
        {
            throw new NotFoundHttpException('Incorrectly formatted query.');
        }

        // Secure the given exp_id
        $this->exp_id = $this->getSecuredExperimentId($exp_id);

        // Get the current experiment object
        $experiment = $this
                        ->getDoctrine()
                        ->getManager()
                        ->getRepository('INRIADylissBundle:Experiment')
                        ->find($this->exp_id);



        // Reconstruct SBML file if asked
        if ($format == 'sbml') {
            // Contact the API and retrieve data from it
            $data = $this->get('inria_dyliss.api_matching_kernel')
                         ->reconstructSBML($this->exp_id);


            // Try to load the file
            try {

                $sbml_file = file_get_contents(
                                $this->getUploadRootDir() . '/' . $this->exp_id . '/output.sbml',
                                FILE_USE_INCLUDE_PATH);

            } catch (\Exception $e) {

                // Print information
                $this->addFlash('danger', 'There is no SBML file for the moment.');

                // Redirect to results view
                return $this->redirectToRoute('inria_dyliss_view_result',
                                              array('exp_id' => $this->exp_id),
                                              301);
            }

            // Modification of the content-type to force the download;
            // Without that, the browser will try to open it.
            $response = new Response();
            $response->setContent($sbml_file);
            $response->headers->set('Content-Type', 'application/force-download');
            $response->headers->set('Content-disposition', 'filename=output_' . $exp_id . '.sbml');
            return $response;
        }

        // Contact the API and retrieve data from it
        $data = $this->get('inria_dyliss.api_matching_kernel')
                     ->getData($this->exp_id,
                               $this->authorized_status[$tab]);


        // Print a warning : duplicated identifiers
        // This error is raised only for "Already matched" tab for now.
        if (isset($data['error']) AND !empty($data['error'])) {

            $this->addFlash('warning',
                            'Duplicated identifiers: '
                            . implode(', ', $data['error'])
                            . '. Note: You can\'t reconstruct a SBML file for now.');
        }

        // Code is easier to read => suppress 'error' key
        $data = $data['associations'];

        // If you want to do sort here,
        // Please see the way to call a class method as a callable here:
        // (and then, take a look to the same thing in the Python API...)
        //uasort($data, array($this, 'my_sorter'));

        // Convert to json, csv if asked
        if ($format == 'json') {

            // Return in json format
            $resp = new Response('{"associations": ' . json_encode($data) . '}');
            $resp->headers->set('Content-Type', 'application/json');
            return $resp;

        } elseif ($format == 'csv') {

            // Force Plain text response...
            $resp = new Response($this->dataToCsv($data));
            $resp->headers->set('Content-Type', 'text/plain');
            return $resp;
        }


        $src_db_prefix = (null !== $experiment->getSrcOrigin()) ? $experiment->getSrcOrigin()->getPrefix() : null;
        $dst_db_prefix = $experiment->getDstOrigin()->getPrefix();

        // If default html format is asked
        switch ($tab) {
            case 'matched' :
                return $this->render('INRIADylissBundle:Result:matched_table.html.twig',
                                     array('association_list' => $data,
                                           'exp_id'           => $this->exp_id,
                                           'tab_name'         => $tab,
                                           'src_db_prefix'    => $src_db_prefix,
                                           'dst_db_prefix'    => $dst_db_prefix));
                break;

            case 'proposal':
                return $this->render('INRIADylissBundle:Result:proposal_table.html.twig',
                                     array('association_list' => $data,
                                           'origin'           => 'word',
                                           'exp_id'           => $this->exp_id,
                                           'tab_name'         => $tab,
                                           'src_db_prefix'    => $src_db_prefix,
                                           'dst_db_prefix'    => $dst_db_prefix));
                break;

            case 'tobematched':
                return $this->render('INRIADylissBundle:Result:tobematched_table.html.twig',
                                     array('association_list' => $data,
                                           'exp_id'           => $this->exp_id,
                                           'tab_name'         => $tab,
                                           'src_db_prefix'    => $src_db_prefix,
                                           'dst_db_prefix'    => $dst_db_prefix));
                break;

            default : //useless because of previous verification
                throw new NotFoundHttpException('Incorrectly formatted query.');
        }
    }


    // TODO: capturer les erreurs d'authentification et ne pas renvoyer la page de login là dedans...
    /**
    * @Security("has_role('ROLE_USER')")
    */
    public function deleteAssociationAction($exp_id, $assoc_id)
    {
        // Do the query and return a basic http request or throw an exception
        return $this->modifyAssociation('delete_association', $exp_id, $assoc_id);
    }


    // TODO: capturer les erreurs d'authentification et ne pas renvoyer la page de login là dedans...
    /**
    * @Security("has_role('ROLE_USER')")
    */
    public function validateAssociationAction($exp_id, $assoc_id)
    {
        // Do the query and return a basic http request or throw an exception
        return $this->modifyAssociation('validate_association', $exp_id, $assoc_id);
    }


     // TODO: capturer les erreurs d'authentification et ne pas renvoyer la page de login là dedans...
    /**
    * @Security("has_role('ROLE_USER')")
    */
    public function getStatisticsAction($exp_id) {
        // This function returns Javascript code used to update pie chart
        // in pages of results.


        // Secure the given exp_id
        $this->exp_id = $this->getSecuredExperimentId($exp_id);

        // Fix used colors
        $colors_per_status = array(
            'validation'      => '#009933', // Green
            'already matched' => '#009933', // Green
            'proposal'        => '#0066FF', // Blue
            'to be matched'   => '#FF6600', // Yellow
            'cancelled'       => '#CC0000', // Red
        );

        $highlighted_colors_per_status = array(
            'validation'      => '#00CC66',
            'already matched' => '#00CC66',
            'proposal'        => '#0099FF',
            'to be matched'   => '#FF9933',
            'cancelled'       => '#FF5050',
        );

        // Contact the API and retrieve data from it
        $data = $this->get('inria_dyliss.api_matching_kernel')
                      ->getStatistics($this->exp_id);
        /* Example of data:
            {
              "already matched": 1,
              "cancelled": 2,
              "proposal": 1,
              "to be matched": 6
            }
        */
        /*
        var data = [
        {% for status, value in stats %}
        {
            value: {{ value }},
            color: "{{ attribute(colors, status) }}",
            highlight: "{{ attribute(highlighted_colors, status) }}",
            label: "{{ status|title }}"
        },
        {% endfor %}
        ];
        */

        // Formating for the canvas element
        $stats = 'var data = [';

        // & : Avoid copy in memory. it's a reference.
        foreach($data as $status_name => &$value) {
            // Label's first letters in upper case
            $stats .= '{ value: ' . $value . ', ';
            $stats .= 'color: "' . $colors_per_status[$status_name] . '", ';
            $stats .= 'highlight: "' . $highlighted_colors_per_status[$status_name] . '", ';
            $stats .= 'label: "' . ucwords($status_name) . '"}, ';
        }
        $stats .= '];';

        // Computation for percentages
        $total_sum = array_sum($data);
        $stats .= 'var data_stats = {';

        foreach($data as $status_name => &$value) {
            $stats .= '"' . ucwords($status_name) . '" : ' . round((($value * 100) / $total_sum), 1) . ', ';
        }
        $stats .= '};';

        // Force Plain text response...
        $resp = new Response($stats);
        $resp->headers->set('Content-Type', 'text/plain');
        return $resp;
    }
}