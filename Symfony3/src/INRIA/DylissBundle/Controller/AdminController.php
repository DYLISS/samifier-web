<?php
// src/INRIA/DylissmBundle/Controller/AdminController.php
namespace INRIA\DylissBundle\Controller;

use INRIA\DylissBundle\Entity\AdminConfig;
use INRIA\DylissBundle\Form\AdminConfigType;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
// Secure methods with annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class AdminController extends Controller
{

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__ . '/../../../../web/' . $this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        $user = $this->container->get('security.token_storage')->getToken()->getUser();

        return 'uploads/' . $user->getUsernameCanonical();
    }

    /**
    * @Security("has_role('ROLE_ADMIN')")
    */
    public function viewAction(Request $request)
    {

        // Get the unique AdminConfig object in database

        $adm_config = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('INRIADylissBundle:AdminConfig')
            ->getCurrentConfig();

        // Create Form
        $form = $this->createForm(AdminConfigType::class, $adm_config);

        // We do the link between Request <-> Form (Hydratation phase)
        // Now, $adm_config contains values taken from user inputs.
        // Now if the form is valid, we can persist entities
        if ($form->handleRequest($request)->isValid()) {

            // We persist entities in database
            try {
                $em = $this->getDoctrine()->getManager();
                // Flush modifications
                $em->flush();

                // Print success
                $this->addFlash('success', 'The form has been submitted.');

            } catch (\Exception $e) {

                // Print error
                $this->addFlash('danger',
                                'The form submission was refused. ' . $e->getMessage());
            }
        }

        // Redirect to the admin page
        // GET query  => the user is incoming to this page
        // POST query => the form is invalid => display new form
        return $this->render('INRIADylissBundle:Admin:index.html.twig',
                             array('form' => $form->createView()));
    }

    /**
    * @Security("has_role('ROLE_ADMIN')")
    */
    public function vacuumAction()
    {
        // Remove all orphans from database
        // Make a vacuum

        // Contact the API and retrieve data from it
        $data = $this->get('inria_dyliss.api_matching_kernel')
                     ->vacuum();

        // Print information
        $this->addFlash('info', 'Vaccum: ' . $data['log']);

        return $this->redirectToRoute('inria_dyliss_view_admin');
    }

    public function execute_system_task($action)
    {
        // Execute the task and return stderr + exit status
        // To get the stderr, we merge stderr to stdout and redirect stdout to /dev/null
        exec("/usr/bin/samifier_admin_commands '$action' 2>&1 1> /dev/null",
             $output,
             $exit_status);

        // Print information
        if ($exit_status != 0)
            $this->addFlash('danger', implode($output));
        else
            $this->addFlash('success', "$action");
    }

    /**
    * @Security("has_role('ROLE_ADMIN')")
    */
    public function databasesAction($action)
    {
        // Execute backup task for databases
        // $action strings are filtered in routing.yml

        // Check if backups dir exists
        try {
            $backup_dir = $this->getUploadRootDir() . '/backups';
            if (!file_exists($backup_dir))
                mkdir($backup_dir, 0755, true);
        } catch (\Exception $e) {
            throw new \Exception('User or temp directory creation failure : ' . $e->getMessage());
        }

        $ret = $this->execute_system_task($action);

        return $this->redirectToRoute('inria_dyliss_view_admin');
    }

    /**
    * @Security("has_role('ROLE_ADMIN')")
    */
    public function servicesAction($action)
    {
        // Restart service
        // matching-kernel.service only for the moment
        // Note: Same as databasesAction
        // but without the verification of the existence of the backup directory...

        $ret = $this->execute_system_task($action);

        return $this->redirectToRoute('inria_dyliss_view_admin');
    }

    public function updateProject($url_archive, $destination_dir)
    {
        // Download and decompress the archive at url $url_archive,
        // to the directory $destination_dir.
        // Return the status code in case of error, 0 otherwise.

        // Get archive name
        $paths = explode('/', $url_archive);
        $archive_name = $paths[count($paths) - 1];

        exec("wget $url_archive -O /tmp/$archive_name 2>&1 1> /dev/null",
             $output,
             $exit_status);

        if ($exit_status != 0) {
            $this->addFlash('danger', implode($output));
            return $exit_status;
        }

        exec("tar xvf /tmp/$archive_name --strip 1 --directory $destination_dir 2>&1 1> /dev/null && rm /tmp/$archive_name 2>&1 1> /dev/null",
             $output,
             $exit_status);

        if ($exit_status != 0) {
            $this->addFlash('danger', implode($output));
            return $exit_status;
        }
        return 0;

    }

    /**
    * @Security("has_role('ROLE_ADMIN')")
    */
    public function updateAction()
    {

        // Update Symfony project files
        $this->updateProject(
            'https://gitlab.inria.fr/DYLISS/samifier-web/repository/reborn/archive.tar.gz',
            '/var/www/html/samifier-web/');

        // Update matching kernel project files
        $this->updateProject(
            'https://gitlab.inria.fr/DYLISS/samifier-matching-kernel/repository/reborn/archive.tar.gz',
            '/var/www/python/samifier-matching-kernel/');

        // Restart matching-kernel service
        $this->execute_system_task('restart_matching_kernel');

        // Regenerate cache
        shell_exec('php /var/www/html/Symfony/bin/console cache:clear --env=prod --no-warmup');
        shell_exec('php /var/www/html/Symfony/bin/console cache:warmup --env=prod');
        shell_exec('php /var/www/html/Symfony/bin/console cache:clear --env=dev --no-warmup');
        shell_exec('php /var/www/html/Symfony/bin/console cache:warmup --env=dev');

        return $this->redirectToRoute('inria_dyliss_view_admin');
    }
}
