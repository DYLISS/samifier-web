<?php
// src/INRIA/DylissBundle/APIMatchingKernel/APIMatchingKernel.php

namespace INRIA\DylissBundle\APIMatchingKernel;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\DependencyInjection\ContainerInterface;

class APIMatchingKernel
{
    /* This service is used to contact the JSON API.
       Don't forget to register it in services.yml file.
    */

    // ContainerInterface
    private $container;

    // API host
    private $api_host;

    // API port
    private $api_port;

    public function __construct(ContainerInterface $container) {
        $this->container = $container;
        $this->api_host  = $container->getParameter('api_host');
        $this->api_port  = $container->getParameter('api_port');
    }

    private function curlQuery($url) {
        // Generic function used to contact the api with the given url
        // This functions returns raw result or throws an exception.
        // PS : https://support.ladesk.com/061754-How-to-make-REST-calls-in-PHP

        $curl = curl_init();

        // Optional Authentication:
        //curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        //curl_setopt($curl, CURLOPT_USERPWD, "username:password");

        // Forge dynamic url according to authorized status by the API
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_PORT, $this->api_port);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        // Force fail on error (Ex: 404, 500 etc...)
        curl_setopt($curl, CURLOPT_FAILONERROR, true);
        $curl_response = curl_exec($curl);

        // Detect if the matching kernel server is up
        // With CURLOPT_FAILONERROR flag, 404, 500 errors are catched here
        if ($curl_response === false) {
            $error = curl_error($curl);
            //$info = curl_getinfo($curl);
            curl_close($curl);

            // Add flash message
            //$this->container->get('session')
            //                ->getFlashBag()
            //                ->add('danger', 'Error : ' . var_export($info));
            //throw new NotFoundHttpException('Error : API could not be contacted.');
            throw new \Exception($error);
        }

        curl_close($curl);
        return $curl_response;
    }


    public function getData($exp_id, $tab) {

        $url = 'http://' . $this->api_host
             . '/get_data/' . $exp_id . '/' . rawurlencode($tab);

        return json_decode($this->curlQuery($url),
                           true);
    }

    public function modifyAssociation($exp_id, $url_api, $assoc_id) {

        $url = 'http://' . $this->api_host
             . '/' . $url_api . '/' . $exp_id . '/' . $assoc_id;

        return json_decode($this->curlQuery($url),
                           true);
    }

    public function checkIntegrity($exp_id, $username, $src_file, $dst_file) {

        $url = 'http://' . $this->api_host
             . '/check_files_integrity/' . $exp_id . '/' . rawurlencode($username) . '/' . $src_file;

        // If destination file is not present
        if (isset($dst_file) AND !empty($dst_file))
            $url .= '/' . $dst_file;

        return json_decode($this->curlQuery($url),
                           true);
    }

    public function getStatistics($exp_id) {

        $url = 'http://' . $this->api_host
             . '/get_statistics/' . $exp_id;

        return json_decode($this->curlQuery($url),
                           true);
    }

    public function populateDatabase($exp_id) {

        $url = 'http://' . $this->api_host
             . '/populate_database/' . $exp_id;

        return json_decode($this->curlQuery($url),
                           true);
    }

    public function uploadMapping($exp_id, $dst_file) {

        $url = 'http://' . $this->api_host
             . '/upload_mapping/' . $exp_id . '/' . $dst_file;

        return json_decode($this->curlQuery($url),
                           true);
    }

    public function getInValidation($exp_id) {

        $url = 'http://' . $this->api_host
             . '/get_in_validation/' . $exp_id;

        return json_decode($this->curlQuery($url),
                           true);
    }

    public function exactIdMatching($exp_id) {

        $url = 'http://' . $this->api_host
             . '/exact_id_matching/' . $exp_id;

        return json_decode($this->curlQuery($url),
                           true);
    }

    public function nameMatching($exp_id, $method, $biological_object_type) {

        $url = 'http://' . $this->api_host
             . '/name_matching/' . $exp_id . '/' . $method . '/' . $biological_object_type;

        return json_decode($this->curlQuery($url),
                           true);
    }

    public function samirMatching($exp_id) {

        $url = 'http://' . $this->api_host
             . '/samir_matching/' . $exp_id;

        return json_decode($this->curlQuery($url),
                           true);
    }

    public function reconstructSBML($exp_id) {

        $url = 'http://' . $this->api_host
             . '/reconstruct_sbml/' . $exp_id;

        return json_decode($this->curlQuery($url),
                           true);
    }

    public function vacuum() {

        $url = 'http://' . $this->api_host
             . '/vacuum';

        return json_decode($this->curlQuery($url),
                           true);
    }

    public function validate($exp_id) {

        $url = 'http://' . $this->api_host
             . '/validate/' . $exp_id;

        return json_decode($this->curlQuery($url),
                           true);
    }

}
