<?php

namespace INRIA\DylissBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

// Use to filter Origin objects
use INRIA\DylissBundle\Repository\OriginRepository;

class ExperimentType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // DON'T FORGET TO HANDLE postUpload EVENT in Entity Experiment class
        // if you add a file field here
        // PS: the validator is in Experiment entity
        // it's a callback named isContentValid()
        $builder
            // Go to SourceFileType.php for customization
            ->add('src_file',    SourceFileType::class, array(
                'label'          => 'Source file to standardize:',
                'required'       => true))
            // Go to SourceFileType.php for customization
            ->add('dst_file',    SourceFileType::class, array(
                'label'          => 'Manual mapping for the source file to standardize (optional):',
                'required'       => false))
            ->add('src_origin', EntityType::class, array(
                'label'          => 'Source database (optional: If you don\'t know leave it to None):',
                'class'          => 'INRIADylissBundle:Origin',
                'choice_label'   => 'name',
                'required'       => false,
                'multiple'       => false,
                'expanded'       => true,
                // Filter Origin objects - Go to OriginRepository.php
                'query_builder'  => function(OriginRepository $repo) {
                    return $repo->getFilteredQueryBuilder();
                },
            ))
            ->add('dst_origin', EntityType::class, array(
                'label'          => 'Destination database:',
                'class'          => 'INRIADylissBundle:Origin',
                'choice_label'   => 'name',
                'required'       => true,
                'multiple'       => false,
                'expanded'       => true,
                // Filter Origin objects - Go to OriginRepository.php
                'query_builder'  => function(OriginRepository $repo) {
                    return $repo->getFilteredQueryBuilder();
                },
            ))
            ->add('save',        SubmitType::class, array(
                'label'          => 'Create the study'))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'INRIA\DylissBundle\Entity\Experiment'
        ));
    }
}
