<?php

namespace INRIA\DylissBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

// Use to filter Origin objects
use INRIA\DylissBundle\Repository\OriginRepository;

class EditExperimentType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // DON'T FORGET TO HANDLE postUpload EVENT in Entity Experiment class
        // if you add a file field here
        // PS: the validator is in Experiment entity
        // it's a callback named isContentValid()
        $builder
            // Go to SourceFileType.php for customization
            ->add('dst_file',    SourceFileType::class, array(
                'label'          => 'Manual mapping for the source file to standardize (required):',
                'required'       => true))
            ->add('save',        SubmitType::class, array(
                'label'          => 'Modify the study'))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'INRIA\DylissBundle\Entity\Experiment'
        ));
    }
}
