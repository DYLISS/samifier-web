<?php

namespace INRIA\DylissBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\FileType;

class SourceFileType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            // Label: workaround to mask this sub form...
            ->add('file', FileType::class,
                  array('label' => ' '))
        ;

        // NO ! This form Type will not inherit from ResourceFileType !
        // Why ? Because IT IS A REAL MESS !!!!!!!!!!
        /*$builder->add('Files', ResourceFileType::class, array(
            'data_class' => 'INRIA\DylissBundle\Entity\SourceFile'
        ));*/
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'INRIA\DylissBundle\Entity\SourceFile'
        ));
    }
}