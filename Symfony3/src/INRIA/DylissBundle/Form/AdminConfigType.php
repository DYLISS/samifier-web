<?php

namespace INRIA\DylissBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class AdminConfigType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $adm_config = $options['data'];

        // if you add a file field here
        $builder->add('numberExperiments', IntegerType::class,
                      array('data' => $adm_config->getNumberExperiments(),
                            'label' => 'Number of experiments per user: '))
                ->add('save', SubmitType::class);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'INRIA\DylissBundle\Entity\AdminConfig'
        ));
    }
}
