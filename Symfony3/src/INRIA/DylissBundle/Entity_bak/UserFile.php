<?php

namespace INRIA\DylissBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UserFile
 *
 * @ORM\Table(name="user_file")
 * @ORM\Entity(repositoryClass="INRIA\DylissBundle\Repository\UserFileRepository")
 */
class UserFile
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    // Many is always the owner of the relation
    /**
    * @ORM\ManyToOne(targetEntity="INRIA\DylissBundle\Entity\User")
    * @ORM\JoinColumn(nullable=false)
    */
    private $user;
    
    // Many is always the owner of the relation
    /**
    * @ORM\ManyToOne(targetEntity="INRIA\DylissBundle\Entity\File")
    * @ORM\JoinColumn(nullable=false)
    */
    private $file;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return UserFile
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }
}

