<?php

namespace INRIA\DylissBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Association
 *
 * @ORM\Table(name="association")
 * @ORM\Entity(repositoryClass="INRIA\DylissBundle\Repository\AssociationRepository")
 */
class Association
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    // Many is always the owner of the relation
    // 1 status per association (already matched, proposal, to be matched, cancelled)
    // hasStatus
    /**
    * @ORM\ManyToOne(targetEntity="INRIA\DylissBundle\Entity\AssociationStatus")
    * @ORM\JoinColumn(nullable=false)
    */
    private $status;
    
    // 1 user per experiment
    // 1 process step per association (exact, word, formula)
    // isFromStep
    /**
    * @ORM\ManyToOne(targetEntity="INRIA\DylissBundle\Entity\AssociationProcessStep")
    * @ORM\JoinColumn(nullable=false)
    */
    private $process_step;
    
    // ManyToMany : The owner of the relation is on the side of the entity that is most often questioned
    // Multiple BiologicalObjects (2) per Association & multiple Associations per BiologicalObject
    // Involve
    /**
    * @ORM\ManyToMany(targetEntity="INRIA\DylissBundle\Entity\BiologicalObject", cascade={"persist"})
    */
    private $biological_objects;
    
    /**
     * @var float
     *
     * @ORM\Column(name="score", type="float")
     */
    private $score;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set score
     *
     * @param float $score
     *
     * @return Association
     */
    public function setScore($score)
    {
        $this->score = $score;

        return $this;
    }

    /**
     * Get score
     *
     * @return float
     */
    public function getScore()
    {
        return $this->score;
    }
}

