<?php

namespace INRIA\DylissBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BiologicalObject
 *
 * @ORM\Table(name="biological_object")
 * @ORM\Entity(repositoryClass="INRIA\DylissBundle\Repository\BiologicalObjectRepository")
 */
class BiologicalObject
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    // Many is always the owner of the relation
    // 1 status per association (already matched, proposal, to be matched, cancelled)
    // hasOrigin
    /**
    * @ORM\ManyToOne(targetEntity="INRIA\DylissBundle\Entity\Origin")
    * @ORM\JoinColumn(nullable=false)
    */
    private $origin;
    
    // ManyToMany : The owner of the relation is on the side of the entity that is most often questioned
    // Multiple BiologicalObjects (2) per Association & multiple Associations per BiologicalObject
    // hasNames
    /**
    * @ORM\ManyToMany(targetEntity="INRIA\DylissBundle\Entity\Name", cascade={"persist"})
    */
    private $names;
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
}

