<?php

//namespace INRIA\DylissBundle\Entity;
use INRIA\DylissBundle\Entity\ResourceFile;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Inherit from ResourceFile
 *
 * @ORM\Entity
 */
class SourceFile extends ResourceFile
{

    public function __construct()
    {
        // Initialize date to current date
        parent::__construct();
    }
}
