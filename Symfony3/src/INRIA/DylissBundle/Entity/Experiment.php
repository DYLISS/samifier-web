<?php

namespace INRIA\DylissBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Debug\Exception\ContextErrorException;
// Secure methods with validators
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Experiment
 *
 * @ORM\Table(name="experiment")
 * @ORM\Entity(repositoryClass="INRIA\DylissBundle\Repository\ExperimentRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Experiment
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @var \boolean
     *
     * @ORM\Column(name="lock_state", type="boolean")
     */
    private $lock_state;

    // Many is always the owner of the relation
    // 1 user per experiment
    // Execute
    /**
    * @ORM\ManyToOne(targetEntity="INRIA\UserBundle\Entity\User")
    * @ORM\JoinColumn(nullable=false)
    */
    private $user;

    // 1 sourceFile per experiment
    /**
    * @ORM\OneToOne(targetEntity="INRIA\DylissBundle\Entity\SourceFile", cascade={"persist", "remove"})
    * @ORM\JoinColumn(nullable=false)
    */
    private $src_file;

    // 1 destinationFile per experiment
    /**
    * @ORM\OneToOne(targetEntity="INRIA\DylissBundle\Entity\SourceFile", cascade={"persist", "remove"})
    * @ORM\JoinColumn(nullable=true)
    */
    private $dst_file;

    // Many is always the owner of the relation
    // 1 origin per experiment
    /**
    * @ORM\ManyToOne(targetEntity="INRIA\DylissBundle\Entity\Origin")
    * @ORM\JoinColumn(nullable=false)
    */
    private $dst_origin;

    // Many is always the owner of the relation
    // 1 origin per experiment
    /**
    * @ORM\ManyToOne(targetEntity="INRIA\DylissBundle\Entity\Origin")
    * @ORM\JoinColumn(nullable=true)
    */
    private $src_origin;

    // ManyToMany : The owner of the relation is on the side of the entity that is most often questioned
    // Multiple files per experiment & multiple experiments per file
    // useSource // TODO ça c pour les destinations
    /**
    /*  @ ORM\ManyToMany(targetEntity="INRIA\DylissBundle\Entity\ResourceFile", cascade={"persist"})
    */
    //private $files;

    // Multiple associations per experiment & multiple experiments per association
    // Validate
    /**
    //* ORM\ManyToMany(targetEntity="INRIA\DylissBundle\Entity\Association", cascade={"persist"})
    */
    //private $associations;

    // Yes it's public because ExperimentNotification have to access to it
    public $experiment_dir;

    public function __construct()
    {
        // Initialize date to current date
        $this->date = new \Datetime();

        // Init the lock_state
        $this->lock_state = FALSE;
    }

    /**
   * @Assert\Callback
   */
    public function isContentValid(ExecutionContextInterface $context)
    {
        // Validator for databases

        if ($this->getDstOrigin() == $this->getSrcOrigin()) {

            $context
                ->buildViolation('Invalid content: Source & destination databases must be different.')
                ->atPath('dst_origin')
                ->addViolation();
        }
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Experiment
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set user
     *
     * @param \INRIA\UserBundle\Entity\User $user
     *
     * @return Experiment
     */
    public function setUser(\INRIA\UserBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \INRIA\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add file
     *
     * @param \INRIA\DylissBundle\Entity\ResourceFile $file
     *
     * @return Experiment
     */
    public function addFile(\INRIA\DylissBundle\Entity\ResourceFile $file)
    {
        $this->files[] = $file;

        return $this;
    }

    /**
     * Remove file
     *
     * @param \INRIA\DylissBundle\Entity\ResourceFile $file
     */
    public function removeFile(\INRIA\DylissBundle\Entity\ResourceFile $file)
    {
        $this->files->removeElement($file);
    }

    /**
     * Get files
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * Set src_file
     *
     * @param \INRIA\DylissBundle\Entity\SourceFile $src_file
     *
     * @return Experiment
     */
    public function setSrcFile(\INRIA\DylissBundle\Entity\SourceFile $src_file)
    {
        $this->src_file = $src_file;

        return $this;
    }

    /**
     * Get src_file
     *
     * @return \INRIA\DylissBundle\Entity\SourceFile
     */
    public function getSrcFile()
    {
        return $this->src_file;
    }

    /**
     * Set dstFile
     *
     * @param \INRIA\DylissBundle\Entity\SourceFile $dstFile
     *
     * @return Experiment
     */
    public function setDstFile(\INRIA\DylissBundle\Entity\SourceFile $dstFile = null)
    {
        $this->dst_file = $dstFile;

        return $this;
    }

    /**
     * Get dstFile
     *
     * @return \INRIA\DylissBundle\Entity\SourceFile
     */
    public function getDstFile()
    {
        return (isset($this->dst_file)) ? $this->dst_file : null;
    }

    /**
     * Set dstOrigin
     *
     * @param \INRIA\DylissBundle\Entity\Origin $dstOrigin
     *
     * @return Experiment
     */
    public function setDstOrigin(\INRIA\DylissBundle\Entity\Origin $dstOrigin = null)
    {
        $this->dst_origin = $dstOrigin;

        return $this;
    }

    /**
     * Get dstOrigin
     *
     * @return \INRIA\DylissBundle\Entity\Origin
     */
    public function getDstOrigin()
    {
        return $this->dst_origin;
    }

    /**
     * Set srcOrigin
     *
     * @param \INRIA\DylissBundle\Entity\Origin $srcOrigin
     *
     * @return Experiment
     */
    public function setSrcOrigin(\INRIA\DylissBundle\Entity\Origin $srcOrigin = null)
    {
        $this->src_origin = $srcOrigin;

        return $this;
    }

    /**
     * Get srcOrigin
     *
     * @return \INRIA\DylissBundle\Entity\Origin
     */
    public function getSrcOrigin()
    {
        return $this->src_origin;
    }

    /**
     * Set lockState
     *
     * @param boolean $lockState
     *
     * @return Experiment
     */
    public function setLockState($lockState)
    {
        $this->lock_state = $lockState;

        return $this;
    }

    /**
     * Get lockState
     *
     * @return boolean
     */
    public function getLockState()
    {
        return $this->lock_state;
    }
}
