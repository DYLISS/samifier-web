<?php

namespace INRIA\DylissBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\HttpFoundation\File\UploadedFile;

// , "destfile" = "DestinationFile"  @ORM\DiscriminatorMap( {"srcfile" = "SourceFile"} )
/**
 * ResourceFile
 *
 * @ORM\Table(name="resource_file")
 * @ORM\Entity(repositoryClass="INRIA\DylissBundle\Repository\FileRepository")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\HasLifecycleCallbacks
 */
abstract class ResourceFile
{
    public $AUTHORIZED_FILE_TYPES = array("txt" => "text/",
                                          "csv" => "text/",
                                          "sbml" => "application/xml",
                                          "xml" => "application/xml");

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="real_name", type="string", length=255, unique=false)
     */
    private $real_name;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="md5_checksum", type="string", length=255, unique=false, nullable=true)
     */
    private $md5_checksum;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="upload_date", type="datetime")
     */
    private $upload_date;

    // Attribute that is not persisted in DB => Form submission
    private $file;


    public function __construct()
    {
        // Initialize date to current date
        $this->upload_date = new \Datetime();
    }

    public function getFile()
    {
        return $this->file;
    }

    public function setFile(UploadedFile $file)
    {
        $this->file = $file;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set realName
     *
     * @param string $realName
     *
     * @return ResourceFile
     */
    public function setRealName($realName)
    {
        $this->real_name = $realName;

        return $this;
    }

    /**
     * Get realName
     *
     * @return string
     */
    public function getRealName()
    {
        return $this->real_name;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ResourceFile
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set uploadDate
     *
     * @param \DateTime $uploadDate
     *
     * @return ResourceFile
     */
    public function setUploadDate($uploadDate)
    {
        $this->upload_date = $uploadDate;

        return $this;
    }

    /**
     * Get uploadDate
     *
     * @return \DateTime
     */
    public function getUploadDate()
    {
        return $this->upload_date;
    }

    /**
     * Set md5Checksum
     *
     * @param string $md5Checksum
     *
     * @return ResourceFile
     */
    public function setMd5Checksum($md5Checksum)
    {
        $this->md5_checksum = $md5Checksum;

        return $this;
    }

    /**
     * Get md5Checksum
     *
     * @return string
     */
    public function getMd5Checksum()
    {
        return $this->md5_checksum;
    }
}

// THESE CLASS ARE HERE FOR THE FORM TYPE GENERATOR ONLY !
// WITHOUT THEM, YOU COULD SEARCH A MOMENT WHY YOU ARE GETTING ERRORS...
// => TROLOLOL !
/**
 * Inherit from ResourceFile
 *
 * @ORM\Entity
 */
class SourceFile extends ResourceFile
{

    public function __construct()
    {
        // Initialize date to current date
        parent::__construct();
    }

}
