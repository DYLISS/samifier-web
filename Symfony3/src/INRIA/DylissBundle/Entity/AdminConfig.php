<?php

namespace INRIA\DylissBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AdminConfig
 *
 * @ORM\Table(name="admin_config")
 * @ORM\Entity(repositoryClass="INRIA\DylissBundle\Repository\AdminConfigRepository")
 */
class AdminConfig
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="number_experiments", type="integer")
     */
    private $numberExperiments;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set numberExperiments
     *
     * @param integer $numberExperiments
     *
     * @return AdminConfig
     */
    public function setNumberExperiments($numberExperiments)
    {
        $this->numberExperiments = $numberExperiments;

        return $this;
    }

    /**
     * Get numberExperiments
     *
     * @return int
     */
    public function getNumberExperiments()
    {
        return $this->numberExperiments;
    }

    public function __construct()
    {
        // Initialize the number of experiments authorized
        $this->numberExperiments = 1;
    }
}
