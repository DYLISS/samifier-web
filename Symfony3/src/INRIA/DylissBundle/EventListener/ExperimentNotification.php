<?php
// src/INRIA/DylissBundle/EventListener/ExperimentNotification.php

namespace INRIA\DylissBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Doctrine\ORM\Event\OnFlushEventArgs;
use INRIA\DylissBundle\Entity\Experiment;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Debug\Exception\ContextErrorException;

class ExperimentNotification
{

    // ContainerInterface
    private $container;

    public function __construct(ContainerInterface $container) {
        $this->container = $container;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__ . '/../../../../web/' . $this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        $user = $this->container->get('security.token_storage')->getToken()->getUser();

        return 'uploads/' . $user->getUsernameCanonical();
    }

    //// Handle events ////
    public function prePersist(LifecycleEventArgs $args)
    {
        //triggered on initial persist

        // Filter Experiment entities

        $experiment = $args->getEntity();

        // Filter Experiment entities
        if (!$experiment instanceof Experiment) {
            return;
        }

        // Initialize the user directory (for the uploaded files before persist())

        try {
            if (!file_exists($this->getUploadRootDir()))
                mkdir($this->getUploadRootDir(), 0755, true);
        } catch (\Exception $e) {
            throw new \Exception('User or temp directory creation failure : ' . $e->getMessage());
        }
    }


    public function preUpdate(LifecycleEventArgs $args)
    {
        // Same as prePersist function
        // Desactivated => we don't want to move files or retest them.
        //$this->prePersist($args);
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        // Filter Experiment entities

        $experiment = $args->getEntity();

        // Filter Experiment entities
        if (!$experiment instanceof Experiment) {
            return;
        }

        // Move the file from temp directory (no one cares where)
        // to experiment directory
        // The file is moved here,
        //BECAUSE THIS entity is persisted AFTER the SourceFile object

        // Initialize the user directory for THIS experiment (i.e for this experiment id)
        // I use this attribute but it could be a temp variable...
        $experiment->experiment_dir = $this->getUploadRootDir() . '/' . $experiment->getId();

        try {
            /* NOTE: if you want to set higher rights => you will have to set umask() default value to 0:
            Ex:
                $oldmask = umask(0);
                mkdir("test", 0777);
                umask($oldmask);
            */
            if (!mkdir($experiment->experiment_dir, 0755, true))
                throw new \Exception('Experiment directory creation failure.');
        } catch (ContextErrorException $e) {
            // File exists
            throw new \Exception('Experiment directory creation failure : Already exists ?');
        } catch (\Exception $e) {
            throw new \Exception('Experiment directory creation failure (not expected) : ' . $e->getMessage());
        }

        // Move UploadedFile Source
        $uploadedFile = $experiment->getSrcFile()->getFile();
        $uploadedFile->move(
            $experiment->experiment_dir . '/',
            $experiment->getSrcFile()->getName()   // Filename
        );

        // Move UploadedFile Destination
        // Verify the handle of empty file !!!
        if (null !== $experiment->getDstFile()) {
            $uploadedFile = $experiment->getDstFile()->getFile();
            $uploadedFile->move(
                $experiment->experiment_dir . '/',
                $experiment->getDstFile()->getName()   // Filename
            );
        }

        // http://doctrine-orm.readthedocs.org/projects/doctrine-orm/en/latest/reference/events.html#postflush
        // Since POSTPERSIST() event is the equivalent of a flush() in SQLA...
        // This entity is NOT already persisted !
        // YES, there is an ID, but the flush() (equivalent of a commit() in SQLA
        // is not done here !
        // PS: postFlush()/preFlush() events don't send the entity flushed => LOLILOOOooOOOoooL
        // PS2: according to the doc... wait, none of these infos are in the doc ><
        // LOLILOOOooOOOoooL
        // TL;DR: WE ARE IN THE FLUSH() HERE !!


        // Check Integrity of uploaded files
        // Contact the API for this.
        // This is done here,
        //BECAUSE WE NEED A VALID experiment ID !
        //
        // PS: we also need files in correct directory

        // Since Experiment is not persisted, the API will do a glob in
        // the directory instead of retrieving the attributes of the experiment.

        $user = $this->container
            ->get('security.token_storage')
            ->getToken()
            ->getUser();

        // TODO: va planter si getDstFile retourne null
        $data = $this->container->get('inria_dyliss.api_matching_kernel')
                                ->checkIntegrity($experiment->getId(),
                                                 $user->getUsernameCanonical(),
                                                 $experiment->getSrcFile()->getName(),
                                                 (null !== $experiment->getDstFile()) ? $experiment->getDstFile()->getName() : null);

        if ($data['error'] == 'error') {
            // Uploaded files have a problem => inform the user & delete experiment

            $flash_bag = $this->container->get('session')
                                         ->getFlashBag();

            if (isset($data['src_file']))
                $flash_bag->add('danger', 'Source file : ' . $data['src_file']);

            if (isset($data['dst_file']))
                $flash_bag->add('danger', 'Destination file : ' . $data['dst_file']);

            // Delete the entity during the next flush()
            $args->getEntityManager()->remove($experiment);
            $args->getEntityManager()->flush();

            throw new \Exception();
        }
    }

    public function postUpdate(LifecycleEventArgs $args)
    {
        // This function is called when a user uploads a new mapping file
        // for the current experiment.
        // If an exception is raised here, the previous mapping file (entity) is not deleted.
        // If zero exception is raised, the previous entity is removed but the file
        // is not removed.
        // Why ?
        // - The deletion of the experiment deletes ALL the experiment's folder.
        // - We can't get the experiment.id in ResourceFileNotification
        // - The user can reupload the same file with the same hash ?
        // EDIT: These steps are done here:
        // - Previous file is deleted if check is ok in Python code
        // - Current bad mapping file is deleted in this func juste
        // before an Exception is rised. (You can do that in Python...)
        // PS: You have to run Python with correct rights...

        // Filter Experiment entities
        $experiment = $args->getEntity();

        // Filter Experiment entities
        if (!$experiment instanceof Experiment) {
            return;
        }

        // Move UploadedFile Destination
        // Verify the handle of empty file !!!
        $experiment_dir = $this->getUploadRootDir() . '/' . $experiment->getId() . '/';

        if (null !== $experiment->getDstFile()) {

            $uploadedFile = $experiment->getDstFile()->getFile();
            $uploadedFile->move(
                $experiment_dir,
                $experiment->getDstFile()->getName()   // Filename
            );
        }

        // Contact the API : check Integrity and import data
        $data = $this->container->get('inria_dyliss.api_matching_kernel')
                                ->uploadMapping($experiment->getId(),
                                                 $experiment->getDstFile()->getName());

        if ($data['error'] == 'error') {
            // Uploaded files have a problem => inform the user & delete experiment

            $flash_bag = $this->container->get('session')
                                         ->getFlashBag();

            if (isset($data['dst_file']))
                $flash_bag->add('danger', 'Destination file : ' . $data['dst_file']);

            // Remove the bad mapping file
            $fs = new Filesystem();
            $fs->remove($experiment_dir . '/' . $experiment->getDstFile()->getName());

            throw new \Exception();
        }
    }

    public function postFlush(PostFlushEventArgs $args)
    {
        // /!\ Deprecated event  if you want to act on entities !
        // We can't get flushed entities here because we have only entityManager.
        // Have fun.

/*
        // Get current user entity
        $user = $this->container
            ->get('security.token_storage')
            ->getToken()
            ->getUser();

        // Get las experiment id for the current user
        $id = $args->getEntityManager()
            ->getRepository('INRIADylissBundle:Experiment')
            ->getLastUserIdExperiment($user);

        // Contact API
        $data = $this->container
            ->get('inria_dyliss.api_matching_kernel')
            ->populateDatabase($id,
                               $user->getUsernameCanonical());

        // Analyse results
        if ($data['error'] == 'error') {
            // Uploaded files have a problem => inform the user & delete experiment

            $flash_bag = $this->container->get('session')
                                         ->getFlashBag();

            if (isset($data['src_file']))
                $flash_bag->add('danger', 'Source file : ' . $data['src_file']);

            if (isset($data['dst_file']))
                $flash_bag->add('danger', 'Destination file : ' . $data['dst_file']);

            // Delete the entity during the next flush()
            $args->getEntityManager()->remove($experiment);

            throw new \Exception();
        }
*/

    }

    public function onFlush(OnFlushEventArgs $eventArgs)
    {

        // http://mightyuhu.github.io/blog/2012/03/27/doctrine2-event-listener-persisting-a-new-entity-in-onflush/


        $em = $eventArgs->getEntityManager();
        $eventManager = $em->getEventManager();
        $uow = $em->getUnitOfWork();

        // Remove event, if we call $this->em->flush() now there is no infinite recursion loop!
        $eventManager->removeEventListener('onFlush', $this);


        foreach($uow->getScheduledEntityInsertions() as $entity) {

            //Get the plain ClassName without Namespace
            //$className = join('', array_slice(explode('\\', get_class($entity)), -1));

            //throw new \Exception($className . $entity->getId());

//             $value = $meta -> getReflectionProperty('myEntityFieldName') -> getValue($entity);

            //... create your Entity...
//             $newEvent = new Event;
//             $newEvent->setName('...');
//             $this -> em -> persist($newEvent);

            // Manual flush
            $em->flush();


            // Changing primitive fields or associations requires you to explicitly
            // trigger a re-computation of the changeset of the affected entity.
            // recalculate changeset, since we might messed up the relations
            $meta = $em->getClassMetadata(get_class($entity));
            $uow->recomputeSingleEntityChangeSet($meta, $entity);


            // Filter Experiment entities
            if (!$entity instanceof Experiment) {
                continue;
            }

            // Get current user entity
            $user = $entity->getUser();

            // Contact API
            $data = $this->container
                ->get('inria_dyliss.api_matching_kernel')
                ->populateDatabase($entity->getId());


            // Analyse results
            if ($data['error'] == 'error') {
                // Uploaded files have a problem => inform the user & delete experiment

                $flash_bag = $this->container->get('session')
                                             ->getFlashBag();

                if (isset($data['src_file']))
                    $flash_bag->add('danger', 'Source file : ' . $data['src_file']);

                if (isset($data['dst_file']))
                    $flash_bag->add('danger', 'Destination file : ' . $data['dst_file']);

                // Delete the entity during the next flush()
                $em->remove($entity);
                $em->flush();

                throw new \Exception();
            }

            // Create the log file
            fopen($this->getUploadRootDir() . '/' . $entity->getId() . '/log.txt', 'w');
        }

/*
        // marche pas... => pas appelé en cas d'update ????
        foreach($uow->getScheduledEntityUpdates() as $key => $entity) {

            $meta = $em->getClassMetadata(get_class($entity));
            $changeSet = $uow->getEntityChangeSet($entity);

             throw new \Exception(get_class($entity));
            // Manual flush
            $em->flush();

            // Filter Experiment entities
            if (!$entity instanceof Experiment) {
                continue;
            }
            throw new \Exception();


            // TODO: va planter si getDstFile retourne null
            $data = $this->container->get('inria_dyliss.api_matching_kernel')
                                    ->upload_mapping($entity->getId());

            if ($data['error'] == 'error') {
                // Uploaded files have a problem => inform the user & delete experiment

                $flash_bag = $this->container->get('session')
                                            ->getFlashBag();

                if (isset($data['dst_file']))
                    $flash_bag->add('danger', 'Destination file : ' . $data['dst_file']);

                // Delete the entity during the next flush()
                $em->remove($entity->getDstFile());
                $em->flush();

                throw new \Exception();
            }

            $uow->computeChangeSet($meta, $entity);
        }*/

        //Re-attach since we're done
        $eventManager->addEventListener('onFlush', $this);
    }

    public function preRemove(LifecycleEventArgs $args)
    {
        // Filter Experiment entities

        $experiment = $args->getEntity();

        // Filter Experiment entities
        if (!$experiment instanceof Experiment) {
            return;
        }

        // We temporary save the experiment directory (which depends of the id)
        $experiment->experiment_dir = $this->getUploadRootDir() . '/' . $experiment->getId();
    }

    public function postRemove(LifecycleEventArgs $args)
    {
        // Filter Experiment entities

        $experiment = $args->getEntity();

        // Filter Experiment entities
        if (!$experiment instanceof Experiment) {
            return;
        }

        // We don't have access to the experiment id here => use the temporary variable
        //var_dump($experiment->experiment_dir);

        // Directory deletion
        $fs = new Filesystem();

        if ($fs->exists($experiment->experiment_dir)) {
            $fs->remove($experiment->experiment_dir);
        }
    }
}
