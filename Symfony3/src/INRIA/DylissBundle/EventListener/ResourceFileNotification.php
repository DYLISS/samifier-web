<?php
// src/INRIA/DylissBundle/EventListener/ResourceFiletNotification.php

namespace INRIA\DylissBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use INRIA\DylissBundle\Entity\SourceFile;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ResourceFileNotification
{

    // ContainerInterface
    private $container;

    public function __construct(ContainerInterface $container) {
        $this->container = $container;
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        // Filter SourceFile entities
        // Verify extension & mimetype of recently uploaded file
        // If all is OK, fix the filename in database

        $srcFile = $args->getEntity();

        // Filter SourceFile entities
        if (!$srcFile instanceof SourceFile) {
            return;
        }

        $client_file = $srcFile->getFile();

        // If there is no file
        if (null === $client_file) {
            throw new \Exception('No file uploaded !');
        }

        // Verify extension
        if (!array_key_exists($client_file->getClientOriginalExtension(), $srcFile->AUTHORIZED_FILE_TYPES))
            throw new \Exception('Bad extension !');

        // Verify mime-type
        // Verify if the authorized substring is in the client string
        // Note that the use of !== false is deliberate;
        // strpos returns either the offset at which the needle string begins in the haystack string,
        // or the boolean false if the needle isn't found. Since 0 is a valid offset and 0 is "falsey",
        // we can't use simpler constructs like !strpos($a, 'are').
        if (strpos($client_file->getMimeType(), $srcFile->AUTHORIZED_FILE_TYPES[$client_file->getClientOriginalExtension()]) === false)
            throw new \Exception("Bad mime type ! " . $client_file->getMimeType());


        // Fix file name in DB
        // We respect file extension but not filenames
        // PS: we could DO THAT BECAUSE EXTENSIONS WERE FILTERED IN A WHITE LIST HERE !
        // $client_file->getClientOriginalName()
        $md5Checksum = md5_file($client_file->getRealPath());
        $srcFile->setName('input_file_' . $md5Checksum . '.' . $client_file->getClientOriginalExtension());

        // Set Real name
        $srcFile->setRealName(htmlspecialchars($client_file->getClientOriginalName(), ENT_QUOTES | ENT_HTML5));

        // Fix md5 checksum in DB
        $srcFile->setMd5Checksum($md5Checksum);
    }

    public function preUpdate(LifecycleEventArgs $args)
    {
        // Same as prePersist function
        $this->prePersist($args);
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        // Filter SourceFile entities
        // Detect null upload

        $srcFile = $args->getEntity();

        // Filter SourceFile entities
        if (!$srcFile instanceof SourceFile) {
            return;
        }

        // If there is no file
        // TODO: verify this function is_null
        if (is_null($srcFile->getFile())) {
            throw new \Exception('No file uploaded !');
        }
    }

    public function postUpdate(LifecycleEventArgs $args)
    {
        // Same as postPersist function
        $this->postPersist($args);
    }
}
